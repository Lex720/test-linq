"""testlinq URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from apps.mascotas import views as mascotas_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # Report builder
    url(r'^report_builder/', include('report_builder.urls')),
    # Mascotas
    url(r'^login/$', mascotas_views.login_user, name='login'),
    url(r'^$', mascotas_views.index, name="home"),
    url(r'^mascotas/$', mascotas_views.index, name='mascotas'),
    url(r'^mascotas/create/$', mascotas_views.create_mascota, name='create_mascota'),
    url(r'^mascotas/edit/(?P<mascota_id>\d+)/$', mascotas_views.editar_mascota, name='editar_mascota'),
    url(r'^mascotas/vote/(?P<mascota_id>\d+)/$', mascotas_views.votar_mascota, name='votar_mascota'),
    url(r'^mascotas/vote_ajax/$', mascotas_views.votar_mascota_ajax, name='votar_mascota_ajax'),
]
