# List of common connections and functions


def validate_form(request):
    for key in request:
        value = request[key]
        if value is None or value == "":
            return False
    return True


def upload_file_verification(file, name):
    formats = ['image/jpg', 'image/jpeg', 'image/png', 'image/bmp']
    if file.content_type in formats:
        pic = 'img/mascotas/{0}-{1}'.format(name, file.name)
        return pic
    else:
        return False


def upload_file(pic, file):
    path = 'static/{0}'.format(pic)
    with open(path, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    return True
