from django.contrib import admin
from .models import (
    Mascota, Detalle
)


admin.site.register(Mascota)
admin.site.register(Detalle)
