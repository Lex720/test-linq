from django import forms
from .models import Mascota
from .models import Detalle


class MascotaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MascotaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Mascota
        fields = ['nickname', 'email']


class DetalleForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DetalleForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Detalle
        fields = ['nombre', 'foto', 'edad', 'pedigree']
