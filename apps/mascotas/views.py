from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.messages import error, success
from django.contrib.auth import authenticate, login

from .models import Mascota
from .models import Detalle
from .forms import MascotaForm
from .forms import DetalleForm


def login_user(request):
    if request.method == "GET":
        return render(request, 'mascotas/login.html')
    else:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            success(request, "Logueado")
            return redirect('home')
        else:
            error(request, "Error")
            return render(request, 'mascotas/login.html', {'username': username, 'password': password})


def index(request):
    # return HttpResponse(request.user.pk)
    mascotas = Mascota.objects.all().order_by('-votos')
    if not mascotas:
        return render(request, 'mascotas/list.html')
    paginator = Paginator(mascotas, 5)
    page = request.GET.get('page')
    try:
        pages = paginator.page(page)
    except PageNotAnInteger:
        pages = paginator.page(1)
    except EmptyPage:
        pages = paginator.page(paginator.num_pages)
    return render(request, 'mascotas/list.html', {'mascotas': mascotas, 'pages': pages})


def create_mascota(request):
    if request.method == 'GET':
        mascota_form = MascotaForm()
        detalle_form = DetalleForm()
        return render(request, 'mascotas/create.html', {'mascota_form': mascota_form, 'detalle_form': detalle_form})
    else:
        mascota_form = MascotaForm(request.POST)
        detalle_form = DetalleForm(request.POST, request.FILES)
        if mascota_form.is_valid():
            nueva_mascota = mascota_form.save()
            if detalle_form.is_valid():
                detalle_mascota = detalle_form.save(commit=False)
                detalle_mascota.mascota_id = nueva_mascota.pk
                detalle_mascota.save()
                success(request, "Mascota inscrita satisfactoriamente")
                response = redirect('mascotas')
                return response
        error(request, "Hay un problema con su informacion por favor verifique")
        return render(request, 'mascotas/create.html', {'mascota_form': mascota_form, 'detalle_form': detalle_form})


def editar_mascota(request, mascota_id):
    mascota = get_object_or_404(Mascota, pk=mascota_id)
    detalle = get_object_or_404(Detalle, mascota_id=mascota_id)
    if request.method == 'GET':
        mascota_form = MascotaForm(instance=mascota)
        detalle_form = DetalleForm(instance=detalle)
        return render(request, 'mascotas/edit.html',
                      {'mascota': mascota, 'detalle': detalle, 'mascota_form': mascota_form,
                       'detalle_form': detalle_form})
    else:
        mascota_form = MascotaForm(request.POST, instance=mascota)
        detalle_form = DetalleForm(request.POST, request.FILES, instance=detalle)
        if mascota_form.is_valid():
            mascota_form.save()
            if detalle_form.is_valid():
                detalle_form.save()
                success(request, "Mascota inscrita satisfactoriamente")
                response = redirect('mascotas')
                return response
        error(request, "Hay un problema con su informacion por favor verifique")
        return render(request, 'mascotas/create.html', {'mascota_form': mascota_form, 'detalle_form': detalle_form})


def votar_mascota(request, mascota_id):
    mascota = get_object_or_404(Mascota, pk=mascota_id)
    count = mascota.votos + 1
    mascota.votos = count
    mascota.save()
    success(request, "Voto registrado exitosamente")
    response = redirect('mascotas')
    return response


def votar_mascota_ajax(request):
    mascota_id = request.POST['mascota_id']
    mascota = get_object_or_404(Mascota, pk=mascota_id)
    count = mascota.votos + 1
    mascota.votos = count
    mascota.save()
    mensaje = 'Voto registrado exitosamente'
    data = {'mensaje': mensaje, 'cuenta': count}
    return JsonResponse(data)