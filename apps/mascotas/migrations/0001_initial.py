# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-09 21:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Detalle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('foto', models.FileField(upload_to='static/img/mascotas/')),
                ('edad', models.SmallIntegerField()),
                ('pedigree', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Mascota',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nickname', models.CharField(max_length=50, unique=True)),
                ('email', models.EmailField(max_length=50, unique=True)),
                ('fecha_publicacion', models.DateTimeField(auto_now_add=True)),
                ('votos', models.SmallIntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='detalle',
            name='mascota',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mascotas.Mascota'),
        ),
    ]
