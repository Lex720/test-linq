from django.db import models


class ModelManager(models.Manager):
    def get_queryset(self):
        return super(ModelManager, self).get_queryset().filter()


class Mascota(models.Model):
    nickname = models.CharField(max_length=50, unique=True)
    email = models.EmailField(max_length=50, unique=True)
    votos = models.SmallIntegerField(default=0)
    fecha_publicacion = models.DateTimeField(auto_now_add=True)

    objects = ModelManager()

    def __str__(self):
        return self.nickname


class Detalle(models.Model):
    mascota = models.OneToOneField(Mascota)
    nombre = models.CharField(max_length=50)
    foto = models.FileField(upload_to='static/img/mascotas/')
    edad = models.SmallIntegerField()
    pedigree = models.BooleanField(default=False)

    def __str__(self):
        return 'ID: {0}, Nombre: {1}'.format(self.mascota, self.nombre)

